#!/bin/bash
# yum install -y   net-tools
yum update -y
# yum install -y   yum-utils
# yum install -y   device-mapper-persistent-data
# yum install -y   lvm2
# yum install -y   open-vm-tools
authconfig --passalgo=sha512 --update
# subscription-manager repos --enable=rhel-7-server-rpms
# subscription-manager repos --enable=rhel-7-server-rh-common-rpms
# yum install cloud-init
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y   docker-ce docker-ce-cli containerd.io
systemctl start docker
systemctl enable docker
# docker run hello-world
# yum provides wget
yum install -y wget
wget https://packages.chef.io/files/stable/chef/12.22.5/el/7/chef-12.22.5-1.el7.x86_64.rpm
rpm -ivh chef-12.22.5-1.el7.x86_64.rpm
#adding extra command for proper working of docker - DevOps Team
gpasswd -a centos docker
echo "Given Permission to Centos to access docker file : " $?
systemctl enable docker
sudo yum install epel-release -y
sudo yum install centos-release-scl -y
sudo yum install rh-python36 -y
scl enable rh-python36 bash
python --version
sudo yum install python3-pip -y
pip3 --version
sudo yum install python3-devel -y
sudo yum groupinstall 'development tools' -y
pip3 install --upgrade --user awscli
aws --version